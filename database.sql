DROP DATABASE IF EXISTS heroku_f46f737cfeee97d;
CREATE DATABASE heroku_f46f737cfeee97d;
USE heroku_f46f737cfeee97d;

DROP TABLE IF EXISTS sessions;

DROP TABLE IF EXISTS users;
CREATE TABLE users (
user_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
user_email VARCHAR(255) NOT NULL,
user_codeEmail VARCHAR(255),
user_emailConfirmed TINYINT NOT NULL,
user_username VARCHAR(32) NOT NULL,
user_fullname VARCHAR(64) NOT NULL,
user_password VARCHAR(255) NOT NULL,
user_passwordSalt VARCHAR(255) NOT NULL,
user_privacy TINYINT NOT NULL,
user_photoUrl VARCHAR(255),
user_creationDate DATETIME
);

DROP TABLE IF EXISTS friendships;
CREATE TABLE friendships (
friendship_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
friendship_requestId BIGINT NOT NULL,
friendship_responseId BIGINT NOT NULL,
friendship_status TINYINT DEFAULT 0,
friendship_creationDate DATETIME
);

DROP TABLE IF EXISTS events;
CREATE TABLE events (
event_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
event_ownerId BIGINT NOT NULL,
event_lat DOUBLE NOT NULL,
event_lng DOUBLE NOT NULL,
event_completeAddress TEXT(255) NOT NULL,
event_title VARCHAR(64) NOT NULL,
event_description TEXT(5000),
event_privacy TINYINT NOT NULL,
event_type TINYINT NOT NULL,
event_date DATE NOT NULL,
event_time TIME NOT NULL,
event_days VARCHAR(7),
event_duration TIME NOT NULL,
event_photoUrl VARCHAR(255),
event_creationDate DATETIME
);

DROP TABLE IF EXISTS assists;
CREATE TABLE assists (
assist_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
assist_eventId BIGINT NOT NULL,
assist_userId BIGINT NOT NULL,
assist_creationDate DATETIME
);

DROP TABLE IF EXISTS notifications;
CREATE TABLE notifications (
notification_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
notification_causativeId BIGINT NOT NULL,
notification_userId BIGINT NOT NULL,
notification_type VARCHAR(255) NOT NULL,
notification_viewed TINYINT NOT NULL,
notification_href VARCHAR(255) NOT NULL,
notification_creationDate DATETIME
);

DROP TABLE IF EXISTS comments;
CREATE TABLE comments (
comment_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
comment_userId BIGINT NOT NULL,
comment_eventId BIGINT NOT NULL,
comment_comment TEXT(500) NOT NULL,
comment_creationDate DATETIME
);