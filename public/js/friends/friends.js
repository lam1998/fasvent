window.onload = function(){
	chargeHeader();
	loadFriends();
}

function loadFriends(){
	$.ajax({
		type: "GET",
		url: "/friends/charge",
		success: function(friends){
			document.getElementById("content").innerHTML = "";
			if(friends != "none"){
				if(friends.length == 0){
					document.getElementById("content").innerHTML = "<p class='box'> No tienes amigos, o al menos en esta página";
				} else {
					for (var i = friends.length - 1; i > - 1; i--) {
						document.getElementById("content").innerHTML += "<a href='/" + friends[i].user_username + "' class='noDecorationLink'><div class='box clickbox'>" + 
						"<img src='"+friends[i].user_photoUrl+"' style='max-width: 100%; height: 50px;'> &nbsp" +
						"<label>"+ friends[i].user_username +"</label></div></a>";
					}
				}
			} else {
				document.getElementById("content").innerHTML = "<p> Debes estar registrado para ver sus amigos";
			}
		}
	})
}