window.onload = function(){
	chargeHeader();
	chargeEventAjax();
}

function chargeEventAjax(){
	var eventId = document.getElementById("eventId").value;

	$.ajax({
		type: "GET",
		url: "/event/charge/"+eventId,
		success: function(data){
			loadEvent(data);
			$.ajax({
				type: "GET",
				url: "/event/comments/charge/"+eventId,
				success: function(data){
					loadComments(data);
				}
			})
		}
	})
}

function loadEvent(data){
	var isAssist = data[1];
	data = data[0];
	var time = data[0].event_time.substring(0, 5);
	var duration = data[0].event_duration.substring(0, 5);

	if(data[0].event_type == 0){
		var date = data[0].event_date.substring(0, 10);
		var datetime = getDatetime(date + " " + time);
		$("#datetime").html("<p style='font-weight: bold'>" + datetime + "</p>");
	} else if(data[0].event_type == 1){
		$("#datetime").html("<p style='font-weight: bold'>" + getDays(data[0].event_days) + time +" hrs.</p><br><p style='font-weight: bold'> Hasta el día "+ getDate(data[0].event_date.substring(0, 10)) +"</p>");
	}

	$("#timeElapsed").html(timeElapsed(data[0].event_creationDate));
	$("#assists").html("Asistencias: " + data[0].assists);
	$("#description").html(getLinks(data[0].event_description));
	$("#completeAddress").html(data[0].event_completeAddress);
	$("#duration").html("<p style='font-weight: bold'>Duración: " + duration + " hrs.</p>");
	document.getElementById("ownerId").value = data[0].event_owner;

	if(isAssist == 0){
		$("#assistForm").css("display","block")
	} else {
		$("#notAssistForm").css("display","block")
	}
	$("#privacy").html(getPrivacy(data[0].event_privacy));
	document.getElementById("assistsDiv").style.display = "block";
	document.getElementById("commentForm").style.display = "block";
}

function commentAjax(){
	document.getElementById("commentSubmit").disabled = "disabled";
	$.ajax({
		type: "POST",
		data: $("#commentForm").serialize(),
		url: "/event/comment",
		success: function(data){
			if(data == "Invalid comment"){
				document.getElementById("errorComment").innerHTML = "El comentario tiene que tener entre 1 y 500 dígitos";
			} else if(data == "Not logged"){
				document.getElementById("errorComment").innerHTML = "Debes estar registrado para comentar";
			} else {
				document.getElementById("commentForm").reset();
				loadComments(data);
			}
			document.getElementById("commentSubmit").disabled = false;
		}
	});
	return false;
}

function loadComments(data){
	var eventId = document.getElementById("eventId").value;
	var sessionId = document.getElementById("sessionId").value;
	document.getElementById("comments").innerHTML = "";
	if(data.length != 0){
		document.getElementById("comments").style.display = "block";
		for (var i = data.length - 1; i > - 1; i--) {
			if(data[i].comment_comment != undefined){
				if(data[i].comment_userId == sessionId){
					document.getElementById("comments").innerHTML += "<div class='box'>"+
					"<a href='/event/comment/delete/"+data[i].comment_id+"/"+eventId+"'><img src='/img/x.png' class='button floatRight' style='border-radius: 50%;' title='Eliminar comentario' width='20px' height='20px'></a>"+
					"<a href='/"+data[i].user_username+"'><img src='"+data[i].user_photoUrl+"' class='leftImgBox' width='50'></a>"+
					"<a class='link' href='/"+data[i].user_username+"'> "+data[i].user_username+"</a>"+
					"<p style='color: gray'>"+timeElapsed(data[i].comment_creationDate)+"</p>"+
					"<br>"+
					"<pre style='max-width: 100%;'>"+getLinks(data[i].comment_comment)+"</pre>"
					"</div>";
				} else {
					document.getElementById("comments").innerHTML += "<div class='box'>"+
					"<a href='/"+data[i].user_username+"'><img src='"+data[i].user_photoUrl+"' class='leftImgBox' width='50'></a>"+
					"<a class='link' href='/"+data[i].user_username+"'> "+data[i].user_username+"</a>"+
					"<p style='color: gray'>"+timeElapsed(data[i].comment_creationDate)+"</p>"+
					"<br>"+
					"<pre style='max-width: 100%;'>"+getLinks(data[i].comment_comment)+"</pre>"
					"</div>";
				}
			}
		}
	}
}

function assistAjax(){
	$.ajax({
		type: "POST",
		url: "/event/assist",
		data: $("#assistForm").serialize(),
		success: function(data){
			if(data == "Already assist"){
				document.getElementById("errorAssist").innerHTML = "Ya asistías a este evento";
			} else if(data == "Not logged"){
				document.getElementById("errorAssist").innerHTML = "Debes estar registrado para poder asistir a eventos";
			} else if(data == "Event over"){
				document.getElementById("errorAssist").innerHTML = "El evento ya ha terminado";
			} else if(data == true){
				location.reload();
			} else {
				document.getElementById("errorAssist").innerHTML = "Error";
			}
		}
	})
	return false;
}

function notAssistAjax(){
	$.ajax({
		type: "POST",
		url: "/event/notAssist",
		data: $("#assistForm").serialize(),
		success: function(data){
			if(data == "Not logged"){
				document.getElementById("errorAssist").innerHTML = "Debes estar registrado para poder asistir a eventos";
			} else if(data == "Event over"){
				document.getElementById("errorAssist").innerHTML = "El evento ya ha terminado";
			} else if(data == true){
				location.reload();
			} else {
				document.getElementById("errorAssist").innerHTML = "Error";
			}
		}
	})
	return false;
}

function editEvent(){
	document.getElementById("editEvent").style.display = "none";
	document.getElementById("editEventForm").style.display = "inline";
	document.getElementById("descriptionEdit").innerHTML = document.getElementById("description").innerHTML;
	document.getElementById("description").style.display = "none";
}

function editEventCancel(){
	document.getElementById("editEvent").style.display = "inline";
	document.getElementById("editEventForm").style.display = "none";
	document.getElementById("description").style.display = "inline";
}

function editEventConfirm(){
	document.getElementById("editEventSubmit").disabled = "disabled";
	$.ajax({
		type: "POST",
		url: "/event/edit",
		data: $("#editEventForm").serialize(),
		success: function(data){
			if(data == "Error"){
				document.getElementById("editEventError").innerHTML = "Error";
			} else if(data == "Error") {
				document.getElementById("editEventError").innerHTML = "Debes estar registrado para editar este evento";
			} else if(data == "Event over"){
				document.getElementById("editEventError").innerHTML = "El evento ya ha terminado, no puedes editarlo";
			} else if(data == "Invalid description"){
				document.getElementById("editEventError").innerHTML = "La descripción no debe superar los 5000 dígitos";
			} else {
				document.getElementById("editEventError").innerHTML = "";

				document.getElementById("editEvent").style.display = "inline";
				document.getElementById("editEventForm").style.display = "none";
				document.getElementById("description").style.display = "inline";
				document.getElementById("description").innerHTML = getLinks(data[0].event_description);
			}
		
			document.getElementById("editEventSubmit").disabled = false;
		}
	})
	return false;
}