window.onload = function(){
	chargeHeader();
}

function signupAjax(){
    document.getElementById("signupSubmit").disabled = "disabled";
    $.ajax({
        type: "POST",
        url: "/signup",
        data: $("#signupForm").serialize(),
        success: function(data){
            if(data != true){
                if(data == "Invalid fullname"){
                    document.getElementById("errorSignupFullname").innerHTML = "Debe tener entre 3 y 64 dígitos. Sólo se permiten letras.";
                } else {
                    document.getElementById("errorSignupFullname").innerHTML = "";
                }
                if(data == "Invalid username"){
                    document.getElementById("errorSignupUsername").innerHTML = "Debe tener entre 4 y 32 dígitos. Sólo se permiten letras, números y guiones.";
                } else {
                    document.getElementById("errorSignupUsername").innerHTML = "";
                }
                if(data == "Used username"){
                    document.getElementById("errorSignupUsedUsername").innerHTML = "Ese nombre de usuario ya está usado.";
                } else {
                    document.getElementById("errorSignupUsedUsername").innerHTML = "";
                }
                if(data == "Invalid email"){
                    document.getElementById("errorSignupEmail").innerHTML = "Email inválido.";
                } else {
                    document.getElementById("errorSignupEmail").innerHTML = "";
                }
                if(data == "Used email"){
                    document.getElementById("errorSignupUsedEmail").innerHTML = "Ese email ya está usado.";
                } else {
                    document.getElementById("errorSignupUsedEmail").innerHTML = "";
                }
                if(data == "Invalid password"){
                    document.getElementById("errorSignupPassword").innerHTML = "Debe tener entre 4 y 255 dígitos.";
                } else {
                    document.getElementById("errorSignupPassword").innerHTML = "";
                }
            } else {
                window.location="/email-sent/signup";
            }
            document.getElementById("signupSubmit").disabled = false;
        }
    })
    return false;
}