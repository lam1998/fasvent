function sendEmailAjax(){
    $.ajax({
        type: "POST",
        url: "/forgotPassword/email",
        data: $("#forgotPasswordForm").serialize(),
        success: function(data){
            if(data == true){
                location.href="/email-sent/forgot";
            } else {
                document.getElementById("error").innerHTML = data;
            }
        }
    });
    return false;
}