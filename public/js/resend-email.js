function resendEmail(){
	document.getElementById("forgotPasswordSubmit").disabled = "disabled";

 	$.ajax({
		type: "POST",
		url: "/resendEmail",
		data: $("#resendEmailForm").serialize(),
		success: function(data){
			document.getElementById("forgotPasswordSubmit").disabled = false;
			if(data == true){
				document.getElementById("resendEmailDiv").style.display = "none";
				document.getElementById("resendEmailOkText").innerHTML = "Se le ha enviado un nuevo email. Por favor revíselo.";
			} else {
				document.getElementById("error").innerHTML = data;
			}
		}
	});
	return false;
}