function chargeHeader(){
	document.getElementById("header").innerHTML = "<a href='/'>"+
	"<img class='floatLeft' id='imglogo' src='/img/logo.png' style='height:calc(100% - 5px)' title='Fasvent'>"+
	"</a>"+
	"<div id='headerRight'><img class='hidden' src='/img/menu.png' id='headerMenuButton' onclick='headerMenu()'></div>";

	document.getElementById("header").innerHTML += "<form class='searchForm' id='searchFriendsHeader' action='/search' method='post'>"+
	"<input type='text' name='value' placeholder='Buscar eventos y usuarios'>"+
	"<input class='button' type='image' src='/img/search.png' width='20px' height='20px' title='Buscar usuarios'>"+
	"</form>";

	if(document.getElementById("sessionId").value != "undefined"){

		$.ajax({
			type: "GET",
			url: "/user/info/"+document.getElementById("sessionId").value,
			success: function(data){
				document.getElementById("header").innerHTML += "<div id='userheader'>"+
				"<a class='floatLeft noDecorationLink button' title='Perfil' href='/"+data[0].user_username+"'>"+
				"<img class='floatLeft centered' height='20px' width='20px' src='"+data[0].user_photoUrl+"'>"+
				"<p class='floatLeft whitetext'>&nbsp"+data[0].user_username+"</p>"+
				"</a>"+
				"<a class='floatLeft noDecorationLink button' id='headerNotifications' href='/notifications' title='Notificaciones'>"+
				"</a>"+
				"</div>";

				$.ajax({
					type: "GET",
					url: "/notifications/number",
					success: function(notifications){
						document.getElementById("headerNotifications").innerHTML = notifications;
					}
				})
			}
		})
		document.getElementById("headerRight").innerHTML += "<a href='/logout' class='button'>"+
		"Salir"+
		"</a>";
	} else {
		document.getElementById("headerRight").innerHTML += "<a class='button' style='margin-right: 5px' href='/login'>Iniciar sesión</a>"+
		"<a class='button' href='/signup'>Registrarse</a>";
	}
}

function headerMenu(){
	$("#headerMenuButton").remove();
	document.getElementById("headerRight").innerHTML += "<img class='hidden' src='/img/menu.png' id='headerMenuButton'>";

	if(document.getElementById("sessionId").value != "undefined"){
		document.getElementById("header").innerHTML += "<div class='above box' id='headerMenuBox'>"+
		"<img class='button' src='/img/x.png' width='30px' height='30px' style='border-radius: 50%; position:relative; left:20px; top:20px; transform: translate(0, -50%); -webkit-transform: translate(-50%, -50%);' onclick='closeHeaderMenu()'>"+
		"<h2 style='position:absolute; top:30px; left:50%; transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%);'> Menú </h2>"+
		"<form class='searchForm' action='/search' method='post'>"+
		"<input type='text' name='value' placeholder='Buscar eventos y usuarios'>"+
		"<input class='button' type='image' src='/img/search.png' width='20px' height='20px' title='Buscar usuarios'>"+
		"</form>"+
		"<a href='/logout' class='button' style='width:calc(100% - 10px);'>Cerrar sesión</a>"+
		"</div>";
	} else {
		document.getElementById("header").innerHTML += "<div class='above box' id='headerMenuBox'>"+
		"<img class='button' src='/img/x.png' width='30px' height='30px' style='border-radius: 50%; position:relative; left:20px; top:20px; transform: translate(0, -50%); -webkit-transform: translate(-50%, -50%);' onclick='closeHeaderMenu()'>"+
		"<h2 style='position:absolute; top:30px; left:50%; transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%);'> Menú </h2>"+
		"<form class='searchForm' action='/search' method='post'>"+
		"<input type='text' name='value' placeholder='Buscar eventos y usuarios'>"+
		"<input class='button' type='image' src='/img/search.png' width='20px' height='20px' title='Buscar usuarios'>"+
		"</form>"+
		"<a class='button' href='/login' style='width:calc(100% - 10px);'>Iniciar sesión</a><a class='button' href='/signup' style='width:calc(100% - 10px);'>Registrarse</a>"+
		"</div>";
	}
}

function closeHeaderMenu(){
	$("#headerMenuButton").remove();
	document.getElementById("headerRight").innerHTML += "<img class='hidden' src='/img/menu.png' id='headerMenuButton' onclick='headerMenu()'>";
	$("#headerMenuBox").remove();
}