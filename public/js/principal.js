function getDatetime(timestamp){
	var date = new Date(timestamp);

	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	var hours = date.getHours();
	var minutes = date.getMinutes();

	month = (month < 10) ? '0' + month : month;
	if(month == 1){
		month = "Ene";
	} else if(month == 2){
		month = "Feb";
	} else if(month == 3){
		month = "Mar";
	} else if(month == 4){
		month = "Abr";
	} else if(month == 5){
		month = "May";
	} else if(month == 6){
		month = "Jun";
	} else if(month == 7){
		month = "Jul";
	} else if(month == 8){
		month = "Ago";
	} else if(month == 9){
		month = "Sep";
	} else if(month == 10){
		month = "Oct";
	} else if(month == 11){
		month = "Nov";
	} else if(month == 12){
		month = "Dic";
	}

	day = (day < 10) ? '0' + day : day;
	hours = (hours < 10) ? '0' + hours : hours;
	minutes = (minutes < 10) ? '0' + minutes : minutes;

	return day + " " + month + " " + year + ", " + hours + ":" + minutes + " hrs.";
}

function getDate(date){
	var date = new Date(date + " 00:00");

	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();

	month = (month < 10) ? '0' + month : month;
	if(month == 1){
		month = "Ene";
	} else if(month == 2){
		month = "Feb";
	} else if(month == 3){
		month = "Mar";
	} else if(month == 4){
		month = "Abr";
	} else if(month == 5){
		month = "May";
	} else if(month == 6){
		month = "Jun";
	} else if(month == 7){
		month = "Jul";
	} else if(month == 8){
		month = "Ago";
	} else if(month == 9){
		month = "Sep";
	} else if(month == 10){
		month = "Oct";
	} else if(month == 11){
		month = "Nov";
	} else if(month == 12){
		month = "Dic";
	}

	day = (day < 10) ? '0' + day : day;

	return day + " " + month + " " + year;
}

function timeElapsed(timestamp){
	var date = new Date(timestamp);
	var startTime = new Date;
	var elapsed = (startTime.getTime() - date.getTime()) / 1000;

	if(elapsed < 60){
		return parseInt(elapsed) + "s";
	} else if(elapsed >= 60 && elapsed < 60*60){
		return parseInt(elapsed/60) + "m";
	} else if(elapsed >= 60*60 && elapsed < 60*60*24){
		return parseInt(elapsed/60/60) + "h";
	} else if(elapsed >= 60*60*24 && elapsed < 60*60*24*365){
		return parseInt(elapsed/60/60/24) + "d";
	} else if(elapsed >= 60*60*24*365){
		return parseInt(elapsed/60/60/24/365) + "a";
	}
}

function getPrivacy(number){
	if(number == 0){
		return "Evento público";
	} else if(number == 1){
		return "Evento privado";
	}
}

function getDays(number){
	number = new String(number);
	if(number.length == 7){
		return "Todos los días ";
	} else {
		number = number.replace("1","Lunes ");
		number = number.replace("2","Martes ");
		number = number.replace("3","Miércoles ");
		number = number.replace("4","Jueves ");
		number = number.replace("5","Viernes ");
		number = number.replace("6","Sábados ");
		number = number.replace("7","Domingos ");
		return number;
	}
}

function getLinks(text){
	var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
	return text.replace(exp,"<a href='$1'>$1</a>"); 
}