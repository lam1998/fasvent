window.onload = function(){
	chargeHeader();
	loadSearchAjax();
}

function loadSearchAjax(){
	var value = document.getElementById("value").value;
	$.ajax({
		type: "GET",
		url: "/search/data/"+value,
		success: function(data){
			chargeSearchUsers(data);
		}
	});
	$.ajax({
		type: "GET",
		url: "/searchAllEvents/"+value,
		success: function(data){
			chargeSearchEvents(data);
		}
	});
}

function chargeSearchUsers(data){
	document.getElementById("usersContent").innerHTML = "";
	if(data[0] == undefined){
		document.getElementById("usersContent").innerHTML = "<p> No se encontraron usuarios";
	} else {
		for (var i = data.length - 1; i > - 1; i--) {
			document.getElementById("usersContent").innerHTML += "<a href='/" + data[i].user_username + "' class='noDecorationLink'><div class='box clickbox'>" + 
			"<img src='"+data[i].user_photoUrl+"' style='max-width: 100%; height: 50px;'> &nbsp" +
			"<label>"+ data[i].user_username +"</label></div></a>";
		}
	}
}

function chargeSearchEvents(data){
	document.getElementById("eventsContent").innerHTML = "";
	if(data[0] == undefined){
		document.getElementById("eventsContent").innerHTML = "<p> No se encontraron eventos";
	} else {
		for (var i = data.length - 1; i > - 1; i--) {
			document.getElementById("eventsContent").innerHTML += "<a href='/event/" + data[i].event_id + "' class='noDecorationLink'><div class='box clickbox'>" + 
			"<img src='"+data[i].event_photoUrl+"' style='max-width: 100%; height: 50px;'> &nbsp" +
			"<label>"+ data[i].event_title +"</label></div></a>";
		}
	}
}