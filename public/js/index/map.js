var map, searchMarker;
var eventMarkers = new Array();

function chargeGoogleMaps(){
	var api = "AIzaSyAsNJPR2Jbzap3AlqXk0nhjmKwB_ErvkQM";
	$.getScript("https://maps.googleapis.com/maps/api/js?key="+api+"&callback=initMap");
}

function initMap(){
	var mapOptions = {
		center: new google.maps.LatLng(-34.62,-58.44),
		zoom: 12,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		mapTypeControl: false,
		streetViewControl: false,
		fullscreenControl: false,
		zoomControlOptions: {
			position: google.maps.ControlPosition.TOP_RIGHT
		}
	}

	map = new google.maps.Map(document.getElementById("map"),mapOptions);
	searchMarker = new google.maps.Marker({});
	chargeEventsAjax("map");

	/*$.getJSON('//freegeoip.net/json/?callback=?', function(data) {
		searchLatLng(data.latitude, data.longitude);
	});*/
}

function searchAddress(){
	var geocoder = new google.maps.Geocoder();
	var address = {
		address: document.getElementById("address").value
	}
	function fn_geocoder(data, status){
		if (status == 'OK'){
			searchMarker.setMap(null);
			var place = data[0].geometry.location;

			document.getElementById("address").value = data[0].formatted_address;
			document.getElementById("createEventLatlng").value = place;

			searchMarker = new google.maps.Marker({
				position: place,
				map: map,
				icon: "img/red_marker.png",
				title: data[0].formatted_address,
				animation: google.maps.Animation.BOUNCE
			})
			map.setCenter(searchMarker.getPosition(place));

			document.getElementById("errorEventAddress").innerHTML = "";

		} else {
			document.getElementById("errorEventAddress").innerHTML = "Dirección incorrecta";
		}
	}

	geocoder.geocode(address, fn_geocoder);
}

function searchLatLng(lat, lng){
	var place = {lat: lat, lng: lng}
	map.setCenter(place);
}

function averageLatLng(data){
	if(data[0] != undefined){
		var averageLat = 0;
		var averageLng = 0;

		for (var i = 0; i < data.length; i++) {
			averageLat = averageLat + data[i].event_lat;
			averageLng = averageLng + data[i].event_lng;
		}

		averageLat = (averageLat/data.length);
		averageLng = (averageLng/data.length);

		searchLatLng(averageLat, averageLng);
	}
}