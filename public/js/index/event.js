function createEventAjax(){
	document.getElementById("createEventSubmit").disabled = "disabled";
	document.getElementById("createEventLoading").style.display = "inline";
	$.ajax({
		type: "POST",
		cache: false,
		contentType: false,
		processData: false,
		data: new FormData($("#createEventForm")[0]),
		url: "/event/create",
		success: function(data){
			if(data == "Invalid title"){
				document.getElementById("errorEventTitle").innerHTML = "El título debe tener entre 3 y 64 dígitos";
			} else {
				document.getElementById("errorEventTitle").innerHTML = "";
			}
			if(data == "Invalid description"){
				document.getElementById("errorEventDescription").innerHTML = "La descripción no debe superar los 5000 dígitos";
			} else {
				document.getElementById("errorEventDescription").innerHTML = "";
			}
			if(data == "Invalid latlng"){
				document.getElementById("errorEventAddress").innerHTML = "La ubicación del evento es inválida";
			} else {
				document.getElementById("errorEventAddress").innerHTML = "";
			}
			if(data == "Invalid date"){
				document.getElementById("errorEventDate").innerHTML = "La fecha es inválida";
				document.getElementById("errorEventDailyDate").innerHTML = "La fecha es inválida";
			} else {
				document.getElementById("errorEventDate").innerHTML = "";
				document.getElementById("errorEventDailyDate").innerHTML = "";
			}
			if(data == "Invalid time"){
				document.getElementById("errorEventTime").innerHTML = "La hora es inválida";
				document.getElementById("errorEventDailyTime").innerHTML = "La hora es inválida";
			} else {
				document.getElementById("errorEventTime").innerHTML = "";
				document.getElementById("errorEventDailyTime").innerHTML = "";
			}
			if(data == "Invalid duration"){
				document.getElementById("errorEventDuration").innerHTML = "La hora es inválida";
			} else {
				document.getElementById("errorEventDuration").innerHTML = "";
			}
			if(data == "Invalid days"){
				document.getElementById("errorEventDays").innerHTML = "Se debe seleccionar mínimo un día";
			} else {
				document.getElementById("errorEventDays").innerHTML = "";
			}
			if(data == "Not logged"){
				document.getElementById("errorEventNotLogged").innerHTML = "Debes estar registrado para crear un evento";
			} else {
				document.getElementById("errorEventNotLogged").innerHTML = "";
			}
			if(data == true){
				chargeEventsAjax("all");

				document.getElementById("createEventForm").reset();
				var date = new Date();
				date.setDate(date.getDate()+1);
				date.setHours(date.getHours()+1);
				document.getElementById("dateCreateEvent").value = date.getDate() + "/" + (date.getMonth() +1) + "/" + date.getFullYear();
				document.getElementById("dailyDateCreateEvent").value = date.getDate() + "/" + (date.getMonth() +1) + "/" + date.getFullYear();
				document.getElementById("timeCreateEvent").value = date.getHours() + ":00";
				document.getElementById("dailyTimeCreateEvent").value = date.getHours() + ":00";
				document.getElementById("durationCreateEvent").value = "02:30";
				document.getElementById("preview").src = "img/default_event.png";
				document.getElementById("uploadEventPicture").value = "false";

				searchMarker.setMap(null);
				document.getElementById("createEvent").style.display = "none";
				document.getElementById("createEventPlus").style.display = "block";
			}
			document.getElementById("createEventSubmit").disabled = false;
			document.getElementById("createEventLoading").style.display = "none";
		}
	});
	return false;
}

function chargeEventsAjax(where){
	$.ajax({
		type: "GET",
		url: "/events/charge",
		success: function(data){
			if(where == "all"){
				loadMenuEvents(data);
				loadMapEvents(data);
			} else if(where == "menu"){
				loadMenuEvents(data);
			} else if(where == "map"){
				loadMapEvents(data);
			}
		}
	})
}

function searchEvent(){
	document.getElementById("eventsDiv").innerHTML = "<p class='box'> Cargando...";
	var value = $("input#searchEventInput").val();
	if(value == ""){
		value = "<undefined>";
	}

	$.ajax({
		type: "GET",
		url: "/searchEvent/" + value,
		success: function(data){
			loadMenuEvents(data);
			loadMapEvents(data);
			averageLatLng(data);
		}
	});
	return false;
}

function deleteEventAjax(){
	$.ajax({
		type: "POST",
		url: "/deleteEvent",
		data: $("#deleteEventForm").serialize(),
		success: function(data){
			loadEvents(data);
		}
	});
}

function previewPicture(input, photo){
	if(input.files && input.files[0]){
		var reader = new FileReader();
		reader.onload = function(e){
			document.getElementById(photo).src = e.target.result;
		};
		reader.readAsDataURL(input.files[0]);
	}
}

function assistAjax(){
	return false;
}

function notAssistAjax(){
	return false;
}

function createEventChangeType(){
	var eventType = document.getElementById("selectTypeCreateEvent").value;
	if(eventType == "Único"){
		document.getElementById("createEventUniqueDiv").style.display = "inline";
		document.getElementById("createEventDailyDiv").style.display = "none";
	} else if(eventType == "Diario" || eventType == "Semanal"){
		document.getElementById("createEventUniqueDiv").style.display = "none";
		document.getElementById("createEventDailyDiv").style.display = "inline";
	}
}