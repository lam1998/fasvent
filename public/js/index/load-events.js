function loadMenuEvents(data){
	document.getElementById("eventsDiv").innerHTML = "";
	var privacy;

	if(data.length != 0){
		for (var i = 0; i < data.length; i++) {
			if(data[i].event_type == 0){
				document.getElementById("eventsDiv").innerHTML += "<div class='box'>" + 
				"<a href='/event/"+data[i].event_id+"' class='noDecorationLink'>"+
				"<img src='"+data[i].event_photoUrl+"' class='leftImgBox' style='height:50px;'>" +
				"</a><a href='/event/"+data[i].event_id+"' class='link'>" + data[i].event_title + "</a>"+
				"<p style='color: gray'>"+timeElapsed(data[i].event_creationDate)+"</p>"+
				"<br><p> Creado por: <a href='/"+data[i].user_username+"' class='link'>"+data[i].user_username+"</a>" +
				"<br><br>"+
				"<a class='link' onclick='searchLatLng("+data[i].event_lat+","+data[i].event_lng+")'> Buscar en el mapa </a>"+
				"<p>" + data[i].event_completeAddress + "</p>"+
				"<p>" + getPrivacy(data[i].event_privacy) + "</p>"+
				"<br><p> Asistencias: "+data[i].assists+
				"<br><p style='font-weight: bold'>"+getDatetime(data[i].event_date.substring(0, 10) + " " + data[i].event_time.substring(0, 5))+"<br><p style='font-weight: bold'> Duración: "+data[i].event_duration.substring(0, 5)+" hrs."+
				"<br><br><pre>" + getLinks(data[i].event_description) + "</pre>"+
				"</div>";
			} else if(data[i].event_type == 1){
				document.getElementById("eventsDiv").innerHTML += "<div class='box'>" + 
				"<a href='/event/"+data[i].event_id+"' class='noDecorationLink'>"+
				"<img src='"+data[i].event_photoUrl+"' class='leftImgBox' style='height:50px;'>" +
				"</a><a href='/event/"+data[i].event_id+"' class='link'>" + data[i].event_title + "</a>"+
				"<p style='color: gray'>"+timeElapsed(data[i].event_creationDate)+"</p>"+
				"<br><p> Creado por: <a href='/"+data[i].user_username+"' class='link'>"+data[i].user_username+"</a>" +
				"<br><br>"+
				"<a class='link' onclick='searchLatLng("+data[i].event_lat+","+data[i].event_lng+")'> Buscar en el mapa </a>"+
				"<p>" + data[i].event_completeAddress + "</p>"+
				"<p>" + getPrivacy(data[i].event_privacy) + "</p>"+
				"<br><p> Asistencias: "+data[i].assists+
				"<br><p style='font-weight: bold'>"+ getDays(data[i].event_days) + data[i].event_time.substring(0, 5)+" hrs.<br><p style='font-weight: bold'> Duración: "+data[i].event_duration.substring(0, 5)+" hrs."+
				"<br><p style='font-weight: bold'> Hasta el día "+ getDate(data[i].event_date.substring(0, 10))+
				"<br><br><pre>" + getLinks(data[i].event_description) + "</pre>"+
				"</div>";
			}
		}
	} else {
		document.getElementById("eventsDiv").innerHTML += "<p class='box'> No se encontraron eventos </p>";
	}
}

function loadMapEvents(data){
	if(eventMarkers[0] != undefined){
		for (var i = 0; i < eventMarkers.length; i++) {
			eventMarkers[i].setMap(null);
		}
	}
	var now = new Date();

	var tomorrow = new Date();
	tomorrow.setDate(tomorrow.getDate() + 1);

	var nextWeek = new Date();
	nextWeek.setDate(nextWeek.getDate() + 7);

	var infowindow = new google.maps.InfoWindow();
	var eventMarker, i, eventDate, markerIcon;

	if(data.length != 0){
		for (var i = 0; i < data.length; i++) {
			eventDate = new Date(data[i].event_date);

			if(data[i].event_type == 0){
				if(eventDate > nextWeek){
					markerIcon = "/img/week_marker.png";
				} else if(eventDate > tomorrow && eventDate < nextWeek){
					markerIcon = "/img/tomorrow_marker.png";
				} else if(eventDate > now){
					markerIcon = "/img/today_marker.png";
				} else {
					markerIcon = "/img/now_marker.png";
				}
			} else if(data[i].event_type == 1){
				markerIcon = "/img/blue_marker.png";
			}

			eventMarker = new google.maps.Marker({
				position: {lat: data[i].event_lat, lng: data[i].event_lng},
				title: data[i].event_title,
				map: map,
				icon: markerIcon
			})

			eventMarkers.push(eventMarker);

			google.maps.event.addListener(eventMarker, 'click', (function(eventMarker, i) {
				return function() {
					if(data[i].event_type == 0){
						infowindow.setContent("<div class='markerContent'>" + 
						"<a href='/event/"+data[i].event_id+"' class='noDecorationLink'>"+
						"<img src='"+data[i].event_photoUrl+"' class='centered' style='height:150px;'>" +
						"</a><a href='/event/"+data[i].event_id+"' class='link centered'>" + data[i].event_title + "</a>"+
						"<br><p> Creado por: <a href='/"+data[i].user_username+"' class='link'>"+data[i].user_username+"</a>" +
						"<p style='color: gray'>"+timeElapsed(data[i].event_creationDate)+"</p>"+
						"<br><p>" + data[i].event_completeAddress + "</p>"+
						"<p>" + getPrivacy(data[i].event_privacy) + "</p>"+
						"<br><p> Asistencias: "+data[i].assists+
						"<br><p style='font-weight: bold'>"+getDatetime(data[i].event_date.substring(0, 10) + " " + data[i].event_time.substring(0, 5))+"<br><p style='font-weight: bold'> Duración: "+data[i].event_duration.substring(0, 5)+" hrs."+
						"<br><br><pre>"+getLinks(data[i].event_description)+"</pre>"+
						"</div>");
					} else if(data[i].event_type == 1){
						infowindow.setContent("<div class='markerContent'>" + 
						"<a href='/event/"+data[i].event_id+"' class='noDecorationLink'>"+
						"<img src='"+data[i].event_photoUrl+"' class='centered' style='height:150px;'>" +
						"</a><a href='/event/"+data[i].event_id+"' class='link centered'>" + data[i].event_title + "</a>"+
						"<br><p> Creado por: <a href='/"+data[i].user_username+"' class='link'>"+data[i].user_username+"</a>" +
						"<p style='color: gray'>"+timeElapsed(data[i].event_creationDate)+"</p>"+
						"<br><p>" + data[i].event_completeAddress + "</p>"+
						"<p>" + getPrivacy(data[i].event_privacy) + "</p>"+
						"<br><p> Asistencias: "+data[i].assists+
						"<br><p style='font-weight: bold'>"+ getDays(data[i].event_days) + data[i].event_time.substring(0, 5)+" hrs.<br><p style='font-weight: bold'> Duración: "+data[i].event_duration.substring(0, 5)+" hrs."+
						"<br><p style='font-weight: bold'> Hasta el día "+ getDate(data[i].event_date.substring(0, 10))+
						"<br><br><pre>"+getLinks(data[i].event_description)+"</pre>"+
						"</div>");
					}
					infowindow.open(map, eventMarker);
				}
			})(eventMarker, i));
		}
	}
}