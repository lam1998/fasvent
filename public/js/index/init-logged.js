function changeMenuDesk(value){
	document.getElementById("events").style.display = "none";
	document.getElementById("about").style.display = "none";
	document.getElementById(value).style.display = "block";

	var li = value + "Li";

	$("#eventsLiDesk").removeClass("activeOptionsLi");
	$("#aboutLiDesk").removeClass("activeOptionsLi");

	$("#"+li+"Desk").addClass("activeOptionsLi");
}

function changeMenuCell(value){
	if(value == "map"){
		document.getElementById("actions").style.display = "none";
		document.getElementById("mapthings").style.display = "inline";
	} else {
		document.getElementById("actions").style.display = "inline";
		document.getElementById("mapthings").style.display = "none";
	}

	document.getElementById("map").style.display = "none";
	document.getElementById("events").style.display = "none";
	document.getElementById("about").style.display = "none";
	document.getElementById(value).style.display = "block";

	var li = value + "Li";

	$("#mapLiCell").removeClass("activeOptionsLi");
	$("#eventsLiCell").removeClass("activeOptionsLi");
	$("#aboutLiCell").removeClass("activeOptionsLi");

	$("#"+li+"Cell").addClass("activeOptionsLi");
}

window.onload = function(){
	chargeHeader();
	var date = new Date();
	date.setDate(date.getDate()+1);
	date.setHours(date.getHours()+1);
	document.getElementById("dateCreateEvent").value = date.getDate() + "/" + (date.getMonth() +1) + "/" + date.getFullYear();
	document.getElementById("dailyDateCreateEvent").value = date.getDate() + "/" + (date.getMonth() +1) + "/" + date.getFullYear();
	document.getElementById("timeCreateEvent").value = date.getHours() + ":00";
	document.getElementById("dailyTimeCreateEvent").value = date.getHours() + ":00";
	document.getElementById("durationCreateEvent").value = "02:30";
	chargeEventsAjax("menu");
	chargeGoogleMaps();
}