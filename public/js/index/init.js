function changeMenuDesk(value){
	document.getElementById("events").style.display = "none";
	document.getElementById("about").style.display = "none";
	document.getElementById(value).style.display = "block";

	var li = value + "Li";

	$("#eventsLiDesk").removeClass("activeOptionsLi");
	$("#aboutLiDesk").removeClass("activeOptionsLi");

	$("#"+li+"Desk").addClass("activeOptionsLi");
}

function changeMenuCell(value){
	if(value == "map"){
		document.getElementById("actions").style.display = "none";
		document.getElementById("mapthings").style.display = "inline";
	} else {
		document.getElementById("actions").style.display = "inline";
		document.getElementById("mapthings").style.display = "none";
	}

	document.getElementById("map").style.display = "none";
	document.getElementById("events").style.display = "none";
	document.getElementById("about").style.display = "none";
	document.getElementById(value).style.display = "block";

	var li = value + "Li";

	$("#mapLiCell").removeClass("activeOptionsLi");
	$("#eventsLiCell").removeClass("activeOptionsLi");
	$("#aboutLiCell").removeClass("activeOptionsLi");

	$("#"+li+"Cell").addClass("activeOptionsLi");
}

window.onload = function(){
	chargeHeader();
	chargeEventsAjax("menu");
	chargeGoogleMaps();
}