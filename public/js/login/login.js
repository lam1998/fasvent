window.onload = function(){
    chargeHeader();
}

function loginAjax(){
    $.ajax({
        type: "POST",
        url: "/login",
        data: $("#loginForm").serialize(),
        success: function(data){
            if(data == true){
                location.reload();
            } else if(data == "Unconfirmed email"){
                window.location="/email-sent";
            } else {
                if(data == "Incorrect email or username"){
                    document.getElementById("errorLogin").innerHTML = "Ese email o nombre de usuario no está registrado";
                } else if(data == "Incorrect password"){
                    document.getElementById("errorLogin").innerHTML = "Contraseña incorrecta";
                } else if(data == "Error"){
                	document.getElementById("errorLogin").innerHTML = "Error";
                } else {
                	document.getElementById("errorLogin").innerHTML = "";
                }
            }
        }
    });
    return false;
}