function chargeEventsAjax(){
	var url = "/profile/events/" + document.getElementById("profileId").value;
	$.ajax({
		type: "GET",
		url: url,
		success: function(data){
			loadEvents(data);
		}
	})
}

function loadEvents(data){
	if(data.length != 0){
		for (var i = data.length - 1; i > - 1; i--) {
			if(data[i].event_type == 0){
				document.getElementById("userEvents").innerHTML += "<div class='box'>" + 
				"<a href='/event/"+data[i].event_id+"' class='noDecorationLink'>"+
				"<img src='"+data[i].event_photoUrl+"' class='leftImgBox' style='height:50px;'>" +
				"</a><a href='/event/"+data[i].event_id+"' class='link'>" + data[i].event_title + "</a>"+
				"<p style='color: gray'>"+timeElapsed(data[i].event_creationDate)+"</p>"+
				"<br><p> Creado por: <a href='/"+data[i].user_username+"' class='link'>"+data[i].user_username+"</a>" +
				"<br><br>"+
				"<a class='link' onclick='searchLatLng("+data[i].event_lat+","+data[i].event_lng+")'> Buscar en el mapa </a>"+
				"<p>" + data[i].event_completeAddress + "</p>"+
				"<p>" + getPrivacy(data[i].event_privacy) + "</p>"+
				"<br><p> Asistencias: "+data[i].assists+
				"<br><p style='font-weight: bold'>"+getDatetime(data[i].event_date.substring(0, 10) + " " + data[i].event_time.substring(0, 5))+"<br><p style='font-weight: bold'> Duración: "+data[i].event_duration.substring(0, 5)+" hrs."+
				"<br><br><pre>" + getLinks(data[i].event_description) + "</pre>"+
				"</div>";
			} else if(data[i].event_type == 1){
				document.getElementById("userEvents").innerHTML += "<div class='box'>" + 
				"<a href='/event/"+data[i].event_id+"' class='noDecorationLink'>"+
				"<img src='"+data[i].event_photoUrl+"' class='leftImgBox' style='height:50px;'>" +
				"</a><a href='/event/"+data[i].event_id+"' class='link'>" + data[i].event_title + "</a>"+
				"<p style='color: gray'>"+timeElapsed(data[i].event_creationDate)+"</p>"+
				"<br><p> Creado por: <a href='/"+data[i].user_username+"' class='link'>"+data[i].user_username+"</a>" +
				"<br><br>"+
				"<a class='link' onclick='searchLatLng("+data[i].event_lat+","+data[i].event_lng+")'> Buscar en el mapa </a>"+
				"<p>" + data[i].event_completeAddress + "</p>"+
				"<p>" + getPrivacy(data[i].event_privacy) + "</p>"+
				"<br><p> Asistencias: "+data[i].assists+
				"<br><p style='font-weight: bold'>"+ getDays(data[i].event_days) + data[i].event_time.substring(0, 5)+" hrs.<br><p style='font-weight: bold'> Duración: "+data[i].event_duration.substring(0, 5)+" hrs."+
				"<br><p style='font-weight: bold'> Hasta el día "+ getDate(data[i].event_date.substring(0, 10))+
				"<br><br><pre>" + getLinks(data[i].event_description) + "</pre>"+
				"</div>";
			}
		}
	}
	document.getElementById("userEvents").style.display = "block";
}

function chargeProfile(){
	var userId = document.getElementById("profileId").value;
	var status = document.getElementById("status").value;
	if(status == "none"){
		$("#content").html("<form method='post' action='/friendRequest/send'>"+
		"<input class='button' type='submit' value='Agregar como amigo'>"+
		"<input type='hidden' value='"+userId+"' name='responseId'>"+
		"</form>");
	} else if(status == "sent"){
		$("#content").html("<form method='post' action='/friendRequest/cancel'>"+
		"<input class='button' type='submit' value='Cancelar solicitud de amistad'>"+
		"<input type='hidden' value='"+userId+"' name='responseId'>"+
		"</form>");
	} else if(status == "received"){
		$("#content").html("<form method='post' action='/friendRequest/accept'>"+
		"<input class='button' type='submit' value='Aceptar solicitud de amistad'>"+
		"<input type='hidden' value='"+userId+"' name='requestId'>"+
		"</form>");
	}
}

function countFriends(){
	var userId = document.getElementById("profileId").value;
	$.ajax({
		type: "GET",
		url: "/countFriends/" + userId,
		success: function(data){
			if(data[0].friends == 1){
				var word = " amigo";
			} else {
				var word = " amigos";
			}
			document.getElementById("countFriends").innerHTML = data[0].friends + word;
			/*$.ajax({
				type: "GET",
				url: "/countMutualFriends/" + userId,
				success: function(data){
					if(data != false){
							if(data[0].mutualFriends == 1){
							var word = " amigo en común)";
						} else {
							var word = " amigos en común)";
						}
						document.getElementById("countFriends").innerHTML += " (" + data[0].mutualFriends + word;
					}
				}
			});*/
		}
	});
}

window.onload = function(){
	chargeHeader();
	chargeEventsAjax();
	chargeProfile();
	countFriends();
}