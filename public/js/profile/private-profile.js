function chargeProfile(){
	var userId = document.getElementById("profileId").value;
	var status = document.getElementById("status").value;
	if(status == "none"){
		$("#content").html("<form method='post' action='/friendRequest/send'>"+
		"<input class='button' type='submit' value='Agregar como amigo'>"+
		"<input type='hidden' value='"+userId+"' name='responseId'>"+
		"</form>");
	} else if(status == "sent"){
		$("#content").html("<form method='post' action='/friendRequest/cancel'>"+
		"<input class='button' type='submit' value='Cancelar solicitud de amistad'>"+
		"<input type='hidden' value='"+userId+"' name='responseId'>"+
		"</form>");
	} else if(status == "received"){
		$("#content").html("<form method='post' action='/friendRequest/accept'>"+
		"<input class='button' type='submit' value='Aceptar solicitud de amistad'>"+
		"<input type='hidden' value='"+userId+"' name='requestId'>"+
		"</form>");
	}
}

function countFriends(){
	var userId = document.getElementById("profileId").value;
	$.ajax({
		type: "GET",
		url: "/countFriends/" + userId,
		success: function(data){
			if(data[0].friends == 1){
				var word = " amigo";
			} else {
				var word = " amigos";
			}
			document.getElementById("countFriends").innerHTML = data[0].friends + word;
			/*$.ajax({
				type: "GET",
				url: "/countMutualFriends/" + userId,
				success: function(data){
					if(data != false){
							if(data[0].mutualFriends == 1){
							var word = " amigo en común)";
						} else {
							var word = " amigos en común)";
						}
						document.getElementById("countFriends").innerHTML += " (" + data[0].mutualFriends + word;
					}
				}
			});*/
		}
	});
}

window.onload = function(){
	chargeHeader();
	chargeProfile();
	countFriends();
}