function newPasswordAjax(){
    $.ajax({
        type: "POST",
        url: "/forgotPassword/newPassword",
        data: $("#forgotPasswordForm").serialize(),
        success: function(data){
            if(data == true){
                location.href="/";
            } else {
                document.getElementById("error").innerHTML = data;
            }
        }
    });
    return false;
}

window.onload = function(){
    chargeHeader();
}