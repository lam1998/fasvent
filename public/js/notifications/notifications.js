window.onload = function(){
	chargeHeader();
	chargeNotificationsAjax();
}

function chargeNotificationsAjax(){
	var userId = document.getElementById("sessionId").value;
	$.ajax({
		type: "GET",
		url: "/notifications/"+userId,
		success: function(data){
			loadNotifications(data);
		}
	})
}

function loadNotifications(data){
	if(data.length == 0){
		document.getElementById("content").innerHTML = "<p class='box'> No tienes notificaciones";
	} else {
		for (var i = data.length - 1; i > - 1; i--) {

			if(data[i].notification_type == "friendRequest"){
				data[i].notification_type = "te ha enviado una solicitud de amistad";
			} else if(data[i].notification_type == "acceptedFriendRequest"){
				data[i].notification_type = "ha aceptado tu solicitud de amistad";
			} else if(data[i].notification_type == "friendEvent"){
				data[i].notification_type = "ha creado un evento";
			} else if(data[i].notification_type == "commentYourEvent"){
				data[i].notification_type = "ha comentado tu evento";
			} else if(data[i].notification_type == "commentEvent"){
				data[i].notification_type = "ha comentado el evento al que asistirás";
			} else if(data[i].notification_type == "deletedEvent"){
				data[i].notification_type = "ha eliminado su evento";
			} else if(data[i].notification_type == "editedEvent"){
				data[i].notification_type = "ha editado su evento";
			}

			var box;
			if(data[i].notification_viewed == 0){
				box = "notificationWhiteBox";
			} else {
				box = "notificationGreenBox";
			}
			document.getElementById("content").innerHTML += "<a href='" + data[i].notification_href + "' class='noDecorationLink'><div class='"+box+"'>" + 
			data[i].user_username + " " +
			data[i].notification_type + "<div style='color: gray;'>"+timeElapsed(data[i].notification_creationDate)+"</div></div></a>";
		}
	}
}