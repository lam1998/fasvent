window.onload = function(){
	chargeHeader();
	chargeConfigInfo();
}

function chargeConfigInfo(){
	if(document.getElementById("sessionId") != undefined){
		$.ajax({
			type: "GET",
			url: "/user/info/"+document.getElementById("sessionId").value,
			success: function(data){
				document.getElementById("usernameConfigInput").value = data[0].user_username;
				if(data[0].user_privacy == 1){
					document.getElementById("privacyConfigInput").value = "Privado";
					document.getElementById("privacyPrivate").selected = "selected";
				} else {
					document.getElementById("privacyConfigInput").value = "Público";
					document.getElementById("privacyPublic").selected = "selected";
				}
			}
		})
	}
}

function changeUsername(){
	$.ajax({
		type: "POST",
		data: $("#changeUsernameForm").serialize(),
		url: "/change/username",
		success: function(data){
			if(data == true){
				location.reload();
			} else {
				if(data == "Used username"){
					document.getElementById("errorChangeUsername").innerHTML = "Ese nombre de usuario ya está usado";
				}
				if(data == "Invalid username"){
					document.getElementById("errorChangeUsername").innerHTML = "Su nombre de usuario es inválido";
				}
				if(data == "Wrong current password"){
					document.getElementById("errorChangeUsername").innerHTML = "Su contraseña actual es inválida";
				}
				if(data == "Error"){
					document.getElementById("errorChangeUsername").innerHTML = "Error";
				}
			}
		}
	});
	return false;
}

function changePassword(){
	$.ajax({
		type: "POST",
		data: $("#changePasswordForm").serialize(),
		url: "/change/password",
		success: function(data){
			if(data == true){
				location.reload();
			} else {
				if(data == "Invalid password"){
					document.getElementById("errorChangePassword").innerHTML = "Su contraseña es inválida";
				}
				if(data == "Wrong current password"){
					document.getElementById("errorChangePassword").innerHTML = "Su contraseña actual es inválida";
				}
				if(data == "Error"){
					document.getElementById("errorChangePassword").innerHTML = "Error";
				}
			}
		}
	});
	return false;	
}

function changePrivacy(){
	$.ajax({
		type: "POST",
		data: $("#changePrivacyForm").serialize(),
		url: "/change/privacy",
		success: function(data){
			if(data == true){
				location.reload();
			} else {
				if(data == "Wrong current password"){
					document.getElementById("errorChangePrivacy").innerHTML = "Su contraseña actual es inválida";
				}
				if(data == "Error"){
					document.getElementById("errorChangePrivacy").innerHTML = "Error";
				}
			}
		}
	});
	return false;
}